<?php
namespace Drupal\hector_test\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

class HectorAdminForm extends ConfigFormBase
{
    public function getFormId()
    {
        return "hector_test_admin";
    }
    
    
    protected function getEditableConfigNames()
    {
        return [
            "hector_test.settings",
        ];
    }
    
    
    public function buildForm(array $form, FormStateInterface $formState = null)
    {
        $config = $this->config("hector_test.settings");
        
        $form["admin_message"] = [
            "#type" => "textfield",
            "#title" => $this->t("Message"),
            "#default_value" => $config->get("admin.message"),
        ];
        
        return parent::buildForm($form, $formState);
    }
    
    
    public function submitForm(array &$form, FormStateInterface $formState)
    {
        $values = $formState->getValues();
        $this->config("hector_test.settings")
            ->set("admin.message", $values["admin_message"])
            ->save();
        
        parent::submitForm($form, $formState);
    }
}