<?php

namespace Drupal\hector_test\Controller;

use Drupal\Core\Controller\ControllerBase;

class HectorController extends ControllerBase
{
    
    public function content() 
    {
        return array (
            '#type' => 'markup',
            '#markup' => $this->t('Hello People!'),
        );
    }
}