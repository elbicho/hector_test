<?php

/**
 * For the "Hector" block...
 * 
 * @Block(
 *      id = "hector_block",
 *      admin_label = @Translation("Hector block")
 * )
 */

namespace Drupal\hector_test\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

class HectorBlock extends BlockBase implements BlockPluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $config = $this->getConfiguration();
        
        if (isset($config['hector_block_name'])) {
            $name = $config['hector_block_name'];
        } else {
            $name = $this->t('Nada!');
        }
        
        return array (
            '#markup' => $this->t('@name Block Yeah!', array(
                '@name' => $name,
            )),
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form = parent::blockForm($form, $form_state);
        
        $config = $this->getConfiguration();
        
        $form['hector_block_name'] = array (
            '#type' => 'textfield',
            '#title' => $this->t('Da Name!'),
            '#description' => $this->t('Who do you want to say hello to?'),
            '#default_value' => isset($config['hector_block_name']) ? $config['hector_block_name'] : '',
        );
        
        return $form;
    }
    
    /**
    * {@inheritdoc}
    */
    public function blockSubmit($form, FormStateInterface $form_state)  
    {
        $this->setConfigurationValue('hector_block_name', $form_state->getValue('hector_block_name'));
    }
    
    /**
    * {@inheritdoc}
    */
    public function defaultConfiguration() {
        $default_config = \Drupal::config('hector_test.settings');
        return array(
            'hector_block_name' => $default_config->get('hector.hector_block_name')
        );
    }
}